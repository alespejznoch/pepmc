#ifndef MODEL_AUTOMATON_HPP
#define MODEL_AUTOMATON_HPP

#include "dbg_msg/dbg_msg.hpp"
#include "almost_equal/almost_equal.hpp"
#include "interval_set/interval_set.hpp"

#include "data_model/Model.h"

#include <vector>
#include <unordered_map>
#include <cassert>

template <typename value_type>
struct model_transition {
	std::vector<std::pair<value_type, value_type>> variable_range;
	std::vector<std::pair<value_type, value_type>> parameter_set;
	///@todo rlly? decimated?
	std::vector<int> destinated_state; // coord of an estimated state
	bool self_loop = false;

	friend std::ostream& operator<<(std::ostream& out, const model_transition& t) {
		for (auto &i : t.variable_range) {
			out << "(" << i.first << "," << i.second << ") ";
		}
		return out;
	}
};


template <typename value_type>
class model_automaton {
	typedef Model<value_type> model_type;

public:
	model_automaton(model_type const & model)
		: m_model(model), dims(m_model.getDims())
	{
		//TODO maybe: Pridat thresholdy z LTL vlastnostii
		
		// rearrange thresholds
		for (size_t i = 0; i < dims; i++){
			m_thresholds.push_back(m_model.getThresholdsForVariable(i));
		}
	}

	vector<model_transition<value_type>> First();
	vector<model_transition<value_type>> Next(std::vector<int> state_coord);

private:
	struct state {
		vector<model_transition<value_type>> transitions;
		std::vector<std::vector<int>> previous_states;
		bool processed = false;
	};

	class hasher {
	public:
		std::size_t operator()(std::vector<int> const& coord) const {
			std::size_t seed = 0;
			for (auto &i : coord) {
				seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2); // 0x9e3779b9 - 32 "random" bits
			}
			return seed;
		}
	};

	size_t ipow(size_t base, size_t exp);

	void ComputeTransitionsForDimension(size_t dim,
		value_type threshold1, value_type threshold2, std::vector<value_type> variables, std::vector<int> state_coord,
		interval<value_type> &down, interval<value_type> &up, interval<value_type> &self, size_t &p_dim);

	void getValueOfNonparamSubmembersForDim(std::vector<value_type> variables, size_t dim, value_type threshold,
		value_type &val, value_type &p_val, size_t &p_dim) const;
	///@todo use structure to unite some of the parameters
	model_transition<value_type> MakeTransition(std::vector<int> original_state,
		size_t dim, value_type parameter_start, value_type parameter_end, size_t p_dim, int up);

	model_type m_model;
	std::vector<std::vector<value_type>> m_thresholds;
	unordered_map<std::vector<int>, state, hasher> m_states;
	size_t dims;
};

// Long double is not allowed
template <>
class model_automaton < long double >;


#include "model_automaton.tpp"

#endif
