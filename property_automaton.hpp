#ifndef PROPERTY_AUTOMATON_HPP
#define PROPERTY_AUTOMATON_HPP

#include "dbg_msg/dbg_msg.hpp"

#include "data_model/Model.h"

#include <queue>
#include <algorithm>

template <typename value_type>
struct property_transition {
	string name2;
	std::size_t id2;

	std::vector<std::pair<value_type, value_type>> variable_range;
};

template <typename value_type>
class property_automaton {
	typedef Model<value_type> model_type;

public:
	property_automaton(model_type const & model)
		: m_model(model), dims(m_model.getDims()) {}

	void initialize(std::istream &in);

	vector<property_transition<value_type>> First();
	vector<property_transition<value_type>> Next(size_t state);

	bool isFinal(size_t state);

private:

	struct state {
		string name;
		std::size_t id;
		bool final;

		vector<property_transition<value_type>> property_transitions;

		bool operator < (const state& _ps) const
		{
			return (id < _ps.id);
		}

		friend std::ostream& operator<<(std::ostream& out, const state& t) {
			for (auto i : t.variable_range) {
				out << "(" << i.first << "," << i.second << ") ";
			}
			return out;
		}
	};

	void trim(std::string& _l);
	std::queue<std::string> split(std::string _separator, std::string _what);
	bool is_comment(std::string _l);

	bool ContainsTransition(state ps, string n, size_t &c);
	bool ContainsTransition(state ps, std::string n);


	model_type m_model;
	size_t dims;
	std::vector<state> m_states;
};

// Long double is not allowed
template <>
class property_automaton < long double >;

#include "property_automaton.tpp"
#endif
