#ifndef DBG_MSG_HPP
#define DBG_MSG_HPP

#include <iomanip>
#include <iostream>
#include <typeinfo>

#define DEBUG true

namespace debug {

extern int indentation;

void WriteIndentation(char c = ' ');

template<class T> void DebugVariable(const std::string& name, const T& value) {
	WriteIndentation();
	std::cout << std::fixed;
	std::cout << std::setprecision(16);
	std::cout << typeid(value).name() << " " << name << " = " << value << std::endl;
}

class log {
public:
	
	log(const std::string& str);
	
	~log();
	
private:
	std::string context;
};
}


#ifdef DEBUG

#define DEBUG_METHOD() debug::log _debugLog(__FUNCTION__);
#define DEBUG_VARIABLE(variable) debug::DebugVariable(#variable, variable);
#define DEBUG_MESSAGE(str)\
	debug::WriteIndentation(); \
	std::cout << std::fixed; \
	std::cout << std::setprecision(4); \
	std::cout << str << std::endl;
#else
#define DEBUG_METHOD() do { } while ( false )
#define DEBUG_VARIABLE() do { } while ( false )
#define DEBUG_MESSAGE(str) do { } while ( false )
#endif


#endif
