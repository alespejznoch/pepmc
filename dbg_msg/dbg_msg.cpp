#include "dbg_msg.hpp"

namespace debug {

int indentation = 1;

void WriteIndentation(char c) {
	std::cout << std::setw(indentation) << c;
}

log::log(const std::string& str) : context(str) {
	WriteIndentation();
	std::cout << "--> " << context << "   ()" << std::endl;
	indentation += 6;
}

log::~log() {
	indentation -= 6 ;
	WriteIndentation(std::uncaught_exception() ? '*' : ' ');
	std::cout << "<-- " << context << "  ~" << std::endl;
}

}
