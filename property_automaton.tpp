#include "property_automaton.hpp"

template <typename value_type>
void property_automaton<value_type>::initialize(std::istream &in) {
	DEBUG_METHOD();

	std::string modelline;
	std::string line;
	//browsing of lines
	for (int lineno = 1; std::getline(in, line); ++lineno) {
		if (is_comment(line)) continue;
		trim(line);
		modelline += line;
	}

	size_t start = modelline.find("process");
	size_t end = modelline.find("system", start);

	if (start == std::string::npos || end == std::string::npos) {
		std::cout << "Bio file error in LTL" << endl;
		exit(1);
	}


	std::string ltl_part = modelline.substr(start, end - start);
	trim(ltl_part);


	DEBUG_MESSAGE("Parsing token: ==" << ltl_part << "==");

	size_t pos;
	std::queue<std::string> fields;
	std::queue<std::string> fields1;

	
	if ((pos = ltl_part.find("process")) != std::string::npos)
	{
		ltl_part.erase(0, 7);
		trim(ltl_part);
		pos = ltl_part.find_first_of('{');
		ltl_part.erase(0, pos + 1);
		ltl_part.erase(ltl_part.size() - 1, 1);
		trim(ltl_part);


		// First we split the string representing the property process 
		// into state-definition part and transition-definition part.
		pos = ltl_part.find("trans");
		std::string procstates = ltl_part.substr(0, pos);
		ltl_part.erase(0, procstates.size() + 5);

		// now we split state definition according to ';'
		fields = split(";", procstates);


		std::vector<std::string> clauses;
		std::vector<std::string> states;

		// now we iterate, and identify individual parts
		while (!fields.empty())
		{
			int mode = 0;
			DEBUG_MESSAGE(fields.front());
			if (fields.front().find("state") == 0)
			{
				fields.front().erase(0, 6);
				mode = 1;
			}

			if (fields.front().find("init") == 0)
			{
				fields.front().erase(0, 5);
				mode = 2;
			}

			if (fields.front().find("accept") == 0)
			{
				fields.front().erase(0, 7);
				mode = 3;
			}

			// now having one part, we split accordin to ',' to get individual states
			size_t init_id;
			state ps;
			string n = "";
			size_t succ_id = 0;
			fields1 = split(",", fields.front());
			while (!fields1.empty())
			{
				switch (mode) {
				case 1:
					ps.name = fields1.front();
					ps.id = succ_id;
					ps.final = false;
					m_states.push_back(ps);

					succ_id++;
					break;
				case 2:
					for (auto it = m_states.begin(); it != m_states.end(); it++)
					{
						n = (*it).name;
						if (0 == n.compare(fields1.front())) {
							init_id = (*it).id;
							(*it).id = 0;
							break;
						}
					}

					for (auto it = m_states.begin(); it != m_states.end(); it++)
					{
						if ((*it).id == 0) {

							(*it).id = init_id;
							break;
						}
					}

					break;
				case 3:
					for (auto it = m_states.begin(); it != m_states.end(); it++)
					{
						n = (*it).name;
						if (0 == n.compare(fields1.front())) {
							(*it).final = true;
							break;
						}
					}

					break;
				}
				fields1.pop();
			}
			fields.pop();
		}

		
		//Here we parse the transition-definition part of the string representing property process.
		ltl_part.erase(ltl_part.size() - 1, 1); // erase terminating ';'				
		fields = split(",", ltl_part);

		while (!fields.empty())
		{
			fields.front().erase(fields.front().size() - 1, 1); // erase terminating '}'
			fields1 = split("{", fields.front());
			pos = fields1.front().find("->");
			std::string sname1 = fields1.front().substr(0, pos);
			trim(sname1);
			std::string sname2 = fields1.front().substr(pos + 2, fields1.front().size() - pos - 2);
			trim(sname2);
			fields1.pop();
			string n = "";
			
			DEBUG_MESSAGE("prop trans:" << sname1 << "->" << sname2);
			if (fields1.empty()) // empty transition ===> true
			{
				for (auto it = m_states.begin(); it != m_states.end(); it++)
				{
					n = (*it).name;
					if (0 == n.compare(sname1)) {
						if (!ContainsTransition((*it), sname2)) {
							property_transition<value_type> pt;
							pt.name2 = sname2;
							string nn = "";
							for (auto m_states_iterator : m_states)
							{
								nn = m_states_iterator.name;
								if (0 == nn.compare(sname2)) {
									pt.id2 = m_states_iterator.id;
									pt.variable_range.resize(dims);
									for (auto &k : pt.variable_range) {
										k.first = -INFINITY;
										k.second = INFINITY;
									}
								}

							}

							((*it).property_transitions).push_back(pt);
						}
						break;
					}
				}

				DEBUG_MESSAGE(" + true");
			}
			else
			{
				fields1.front().erase(fields1.front().size() - 1, 1); // erase terminating ';'
				fields1.front().erase(0, 6); // erase leading 'guard '
				std::string aux = fields1.front();
				std::string literal;
				fields1.pop();
				fields1 = split("&&", aux);

				while (!fields1.empty())
				{

					//..... parsing Atomic Proposition ....
					if (fields1.front().find("!") != std::string::npos)
					{
						std::cerr << "Atomic proposition contains !, which is not allowed." << std::endl;
						exit(1);
					}

					if (fields1.front().find("==") != std::string::npos)
					{
						std::cerr << "Atomic proposition contains ==, which is not allowed." << std::endl;
						exit(1);
					}

					bool negated = false;
					//affine_ap_t AP;
					std::string APstr = fields1.front();

					//Remove possible ()
					if (APstr.find("(") != std::string::npos)
					{
						if (APstr.find("(") != 0 || APstr.find(")") != APstr.size() - 1)
						{
							cout << "cannot parse atomic proposition " + APstr;
						}
						APstr.erase(0, 1);
						APstr.erase(APstr.size() - 1, 1);
					}
					trim(APstr);

					//Test for true statement
					if (APstr.find("true") != std::string::npos ||
						APstr.find("True") != std::string::npos ||
						APstr.find("TRUE") != std::string::npos
						)
					{

						for (auto it : m_states)
						{
							n = it.name;
							if (0 == n.compare(sname1)) {
								if (!ContainsTransition(it, sname2)) {
									property_transition<value_type> pt;
									pt.name2 = sname2;

									string nn = "";
									for (auto &m_states_iterator : m_states)
									{
										nn = m_states_iterator.name;
										if (0 == nn.compare(sname2)) {
											pt.id2 = m_states_iterator.id;
											pt.variable_range.resize(dims);
											for (auto &k : pt.variable_range) {
												k.first = -INFINITY;
												k.second = INFINITY;
											}
										}

									}
									(it.property_transitions).push_back(pt);
								}
								break;
							}
						}

						fields1.pop();
						continue;
					}

					if (APstr.find("not") == 0)
					{
						APstr.erase(0, 4);
						negated = true;
					}
					std::string text_repr = APstr;
					pos = APstr.find_first_of("!>=<");
					literal = APstr.substr(0, pos);
					trim(literal);
					//size_t var = get_varid(aux);

					APstr.erase(0, pos);
					pos = APstr.find_first_not_of("!>=<");
					aux = APstr.substr(pos, APstr.size() - pos);
					trim(aux);
					APstr.erase(pos, APstr.size() - pos);
					trim(APstr);


					for (auto it = m_states.begin(); it != m_states.end(); it++)
					{
						n = (*it).name;
						if (0 == n.compare(sname1)) {
							size_t c = 0;
							if (ContainsTransition((*it), sname2, c)) {
								


								if (APstr.size() == 2 && std::string::npos != APstr.find(">="))
								{
									negated = !negated;
								}

								if (APstr.size() == 1 && std::string::npos != APstr.find(">"))
								{
									negated = !negated;
								}


								//
								size_t dim = m_model.getVariableIndex(literal);
								value_type value = atof(aux.c_str());

								if (negated) {
									(*it).property_transitions.at(c).variable_range.at(dim).second = value;
								}
								else {
									(*it).property_transitions.at(c).variable_range.at(dim).first = value;
								}
								

							}
							else {
								property_transition<value_type> pt;
								pt.name2 = sname2;

								string nn = "";
								for (auto m_states_iterator : m_states)
								{
									nn = m_states_iterator.name;
									if (0 == nn.compare(sname2)) {
										pt.id2 = m_states_iterator.id;
									}

								}

								// APstr contains ">="
								if (APstr.size() == 2 && std::string::npos != APstr.find(">="))
								{
									negated = !negated;
								}

								// APstr contains ">"
								if (APstr.size() == 1 && std::string::npos != APstr.find(">"))
								{
									negated = !negated;
								}

								size_t dim = m_model.getVariableIndex(literal);
								value_type value = atof(aux.c_str());

								pt.variable_range.resize(dims);
								for (auto &k : pt.variable_range) {
									k.first = -INFINITY;
									k.second = INFINITY;
								}

								if (negated) {
									pt.variable_range.at(dim).first = value;
								}
								else {
									pt.variable_range.at(dim).second = value;
								}

								((*it).property_transitions).push_back(pt);
							}
							break;
						}
					}
					fields1.pop();
				}
			}
			fields.pop();
		}
	}
	std::sort(m_states.begin(), m_states.end());	
}
/*
template <typename value_type>
vector<property_transition<value_type>> property_automaton<value_type>::First() {
	DEBUG_METHOD();
	*/
	
template <typename value_type>
vector<property_transition<value_type>> property_automaton<value_type>::First() {
	DEBUG_METHOD();

	property_transition<value_type> pt;
	pt.id2 = 0;
	pt.variable_range.resize(dims);
	for (auto &i : pt.variable_range) {
		i.first = -INFINITY;
		i.second = INFINITY;
	}

	vector<property_transition<value_type>> property_transitions;
	property_transitions.push_back(pt);
	return property_transitions;
}
	
		/*
	return m_states.at(0).property_transitions;
}
*/
template <typename value_type>
vector<property_transition<value_type>> property_automaton<value_type>::Next(size_t state) {
	//DEBUG_METHOD();

	return m_states.at(state).property_transitions;
}

template <typename value_type>
bool property_automaton<value_type>::isFinal(size_t state) {
	return m_states.at(state).final;
}

//////////////////////////////////////////////////
////////                  Private definitions
//////////////////////////////////////////////////
// Should remove leading and terminating white space
template <typename value_type>
void property_automaton<value_type>::trim(std::string& _l) {
	if (!_l.empty()) {
		_l.erase(_l.find_last_not_of(" \t\n") + 1);
		if (!_l.empty()) {
			size_t p = _l.find_first_not_of(" \t\n");
			if (p != std::string::npos)
				_l.erase(0, p);
		}
	}
}



// Splits a string according to a separator. The result is stored in a queue of strings.
template <typename value_type>
std::queue<std::string> property_automaton<value_type>::split(std::string _separator, std::string _what) {
	std::queue<std::string> retval;
	while (!_what.empty()) {
		size_t pos = _what.find(_separator);
		std::string s;
		if (pos == std::string::npos) {
			s = _what;
			_what.clear();
		} else {
			s = _what.substr(0, pos - (_separator.size() - 1));
			_what.erase(0, pos + _separator.size());
		}
		trim(s);
		retval.push(s);
		//std::cout <<"split:"<<s<<" _what="<<_what<<std::endl;
	}
	return retval;
}

// Should return true if the string starts with '#' or "//"
template <typename value_type>
bool property_automaton<value_type>::is_comment(std::string _l) {
	trim(_l);
	if ((_l[0] == '#') ||
		(_l[0] == '/' && _l[1] == '/')) {
		return true;
	}
	return false;
}

template <typename value_type>
bool property_automaton<value_type>::ContainsTransition(state ps, std::string n, size_t &c) {
	c = 0;
	for (auto &i : ps.property_transitions) {
		if (0 == i.name2.compare(n)) {
			return true;
		}
		c++;
	}
	return false;
}

template <typename value_type>
bool property_automaton<value_type>::ContainsTransition(state ps, std::string n) {
	for (auto &i : ps.property_transitions) {
		if (0 == i.name2.compare(n)) {
			return true;
		}
	}
	return false;
}
