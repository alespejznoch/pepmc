#include "model_automaton.hpp"

//////////////////////////////////////////////////
////////                  public definitions
//////////////////////////////////////////////////
template <typename value_type>
vector<model_transition<value_type>> model_automaton<value_type>::First() {
	DEBUG_METHOD();

	vector<model_transition<value_type>> results_transitions;
	std::vector <std::pair<value_type, value_type>> init_values(m_model.getInitsValues());
	
	
	typedef typename std::vector<std::pair<value_type, value_type>>::iterator inits_iter;
	typedef typename std::vector<std::vector<value_type>>::iterator threshs_iter;
	int dim = 0;
	int region = 0;

	struct init {
		value_type start;
		value_type end;
		size_t coord;
	};
	std::vector<std::vector<init>> initial_space;
	///@todo divide the code to more functions
	// Iterate dimensions of thresholds and initial values together
	for (std::pair<inits_iter, threshs_iter> iter(init_values.begin(), m_thresholds.begin());
		iter.first != init_values.end() || iter.second != m_thresholds.end();
		iter.first++, iter.second++) {

		auto it_inits = iter.first;
		auto it_threshs = iter.second;

		value_type start = it_inits->first;
		value_type end = it_inits->second;
		region = 0;
		///@todo rename
		bool inside = false;//new to add
		size_t last_coord;
		size_t token = 0;
		vector<init> init_vec;

		///@todo one loop alg
		for (auto it_threshold = it_threshs->begin(); it_threshold != it_threshs->end(); it_threshold++) {
			///@todo english
			// First occurrence of a threshold bigger then start initial value founded,
			// so we store the coord here and use it, if any threshold wouldnt be in initial range
			if ((!token) && (start < (*it_threshold))) {
				token = region;
			}
			if (start<(*it_threshold) && end>(*it_threshold)) {
				inside = true; // threshold exists in range of inits values
				last_coord = region;
				init t; t.start = start; t.end = *it_threshold; t.coord = region;
				init_vec.push_back(t);

				start = (*it_threshold);
			}
			region++;
		}
		
		// If threshold exists inside a init range,
		// we have to add the last one
		if (inside) {
			init t; t.start = start; t.end = end; t.coord = last_coord+1;
			init_vec.push_back(t);
		}
		// Else whole init range is a state
		else {
			init t; t.start = start; t.end = end; t.coord = token;
			init_vec.push_back(t);
		}
		initial_space.push_back(init_vec);
		dim++;
	}

	// Process inits
	// Initialize first transition
	std::vector<model_transition<value_type>> temp_transitions;
	model_transition<value_type> first_transition;
	temp_transitions.push_back(first_transition);

	dim = 0;
	// For each dimension
	for (auto &i : initial_space) {
		std::vector<model_transition<value_type>> new_transitions;
		// For each initial interval in that dimension
		for (auto &j : i) {
			size_t temp_coord = 0;
			// For each stored transition
			for (auto &k : temp_transitions) {
				model_transition<value_type> t = k;///@todo omg

				// Add variable_range
				std::pair<value_type, value_type> temp_interval;
				temp_interval.first = j.start;
				temp_interval.second = j.end;
				t.variable_range.push_back(temp_interval);

				t.destinated_state.push_back(j.coord);		

				new_transitions.push_back(t);
				//DEBUG_MESSAGE("STORED TRANSITION: " << k << "   new transition " << t);
			}
		}
		temp_transitions.clear();
		temp_transitions = new_transitions;
		new_transitions.clear();
		dim++;
	}	

	
	
	for (auto &i : temp_transitions) {
		i.parameter_set.resize(m_model.getParamRanges().size());
		size_t d = 0;
		for (auto &j : i.parameter_set) {
			j.first = m_model.getParamRange(d).first;
			j.second = m_model.getParamRange(d).second;
			d++;
		}
	}
	


	results_transitions = temp_transitions;

	return results_transitions;
}


template <typename value_type>
vector<model_transition<value_type>> model_automaton<value_type>::Next(std::vector<int> state_coord) {
	//DEBUG_METHOD();
	if (state_coord.at(0)==6 && state_coord.at(1)==2)
	{
		std::cout << "target" << std::endl;
	}
	bool add_self = true;
	typename unordered_map<std::vector<int>, state, hasher>::const_iterator got = m_states.find(state_coord);
	if (got == m_states.end()) {
		// make a state

		state st;
		m_states.emplace(state_coord, st);
	}//TODO: optimize?
	else if (true == m_states.at(state_coord).processed) {
		return (m_states.at(state_coord).transitions);
	}

	assert(state_coord.size() == dims);

	std::vector<interval<value_type>> self;
	size_t params_number = m_model.getParamNames().size();
	self.resize(params_number);
	
	std::vector<value_type> vertice;
	vector<model_transition<value_type>> resulting_transitions;
	for (size_t computed_dimension = 0; computed_dimension < dims; computed_dimension++) {

		// updated ranges
		interval<value_type> down;
		interval<value_type> up;
		interval<value_type> temp_self;
		size_t param_index;

		for (size_t variation = 0; variation < ipow(2, dims); variation++) {

			// For processed dimension dont compute variation
			size_t bit_position = ipow(2, computed_dimension);
			if (variation & bit_position) {
				continue;
			}

			vertice.clear();
			vertice.resize(dims);
			for (size_t i = 0; i < dims; i++){
				size_t bit_position = ipow(2, i);

				// If on i-th position is 1
				int t = state_coord.at(i);
				if (variation & bit_position) {
					if (t > (int)m_thresholds.at(i).size() - 1) {
						vertice.at(i) = INFINITY;
					}
					else {
						vertice.at(i) = m_thresholds.at(i).at(t);
					}
				}
				else {
					if (t - 1 < 0) {
						vertice.at(i) = -INFINITY;
					}
					else {
						vertice.at(i) = m_thresholds.at(i).at(t-1);
					}
				}
			}
			// Handle the edges of the parameter space
			value_type t1, t2;
			int t = state_coord.at(computed_dimension);
			if ( t - 1 < 0) {
				t1 = -INFINITY;
			}
			else {
				t1 = m_thresholds.at(computed_dimension).at(t-1);
			}

			if (t > (int)m_thresholds.at(computed_dimension).size() - 1) {
				t2 = INFINITY;
			}
			else {
				t2 = m_thresholds.at(computed_dimension).at(t);
			}
			vector<model_transition<value_type>> temp_transitions;
			
			ComputeTransitionsForDimension(computed_dimension, t1, t2, vertice, state_coord, down, up, temp_self, param_index);


			//resulting_transitions.insert(resulting_transitions.begin(), temp_transitions.begin(), temp_transitions.end());
		}

		value_type up_parameter_1;		value_type up_parameter_2;
		value_type down_parameter_1;	value_type down_parameter_2;

		up.getInterval(up_parameter_1, up_parameter_2);
		down.getInterval(down_parameter_1, down_parameter_2);

		if (up.isSet()) {
			resulting_transitions.push_back(MakeTransition(state_coord, computed_dimension, up_parameter_1, up_parameter_2, param_index, 1));
		}

		if (down.isSet()) {
			resulting_transitions.push_back(MakeTransition(state_coord, computed_dimension, down_parameter_1, down_parameter_2, param_index, -1));
		}

		if (temp_self.isSet() && add_self) {
			// intersection of self and temp_self
			interval<value_type> & i_self = self.at(param_index);

			if (!i_self.isSet()) {
				i_self.setInterval(temp_self.start, temp_self.end);
			}
			else {
				value_type self_start;	value_type self_end;
				value_type temp_self_start;	value_type temp_self_end;
				i_self.getInterval(self_start, self_end);

				temp_self_start = max(temp_self_start, self_start);
				temp_self_end = max(temp_self_end, self_end);
				temp_self.getInterval(temp_self_start, temp_self_end);


			}
		} else {
			add_self = false;
		}


	}


	
	model_transition<value_type> mt;
	bool t = true;
	//if (self.size()!= p_dims)
	for (size_t i = 0; i < self.size(); i++) {
		interval<value_type> &i_self = self.at(i);
		if (!i_self.isSet()) {
			t = false;
			break;
		}
		
		mt.parameter_set.resize(params_number);
		mt.parameter_set.at(i).first = i_self.start;
		mt.parameter_set.at(i).second = i_self.end;
	}

	if (t) {
		mt.destinated_state = state_coord;

		mt.variable_range.resize(dims);
		size_t d;
		d = 0;
		for (auto &i : mt.variable_range) {
			if (state_coord.at(d) <= 0) {
				i.first = -INFINITY;
			}
			else {
				i.first = m_model.getThresholdsForVariable(d).at(state_coord.at(d) - 1);// TODO: m_thresholds?
			}

			if (state_coord.at(d) + 1 >= (int)m_model.getThresholdsForVariable(d).size()) {
				i.second = INFINITY;
			}
			else {
				i.second = m_model.getThresholdsForVariable(d).at(state_coord.at(d));
			}
			d++;
		}

		mt.self_loop = true;

		resulting_transitions.push_back(mt);
	}

	/*
	
	vector<size_t> to_remove;
	size_t tr = 0;
	for (auto &transition : resulting_transitions) {
		for (auto previous_state : m_states.at(state_coord).previous_states) {
			if ((previous_state!=state_coord) && (transition.destinated_state == previous_state) ){
				DEBUG_MESSAGE("HA");
				// transition.parameter_set
				bool cct = false;
				bool pst = false;
				size_t cc_i = 0;
				size_t ps_i = 0;
				for (cc_i = 0; cc_i < m_states.at(state_coord).transitions.size(); cc_i++) {
					if (m_states.at(state_coord).transitions.at(cc_i).self_loop) {
						cct = true;
						break;
					}
				}
				for (ps_i = 0; ps_i < m_states.at(previous_state).transitions.size(); ps_i++) {
					if (m_states.at(previous_state).transitions.at(ps_i).self_loop) {
						pst = true;
						break;
					}
				}

				for (size_t i = 0; i < params_number; i++) {
					interval<value_type> a;
					interval<value_type> b;
					if (cct) {
						a.start = m_states.at(state_coord).transitions.at(cc_i).parameter_set.at(i).first;
						a.end = m_states.at(state_coord).transitions.at(cc_i).parameter_set.at(i).second;
					}

					if (pst) {				
						b.start = m_states.at(previous_state).transitions.at(ps_i).parameter_set.at(i).first;
						b.end = m_states.at(previous_state).transitions.at(ps_i).parameter_set.at(i).second;
					}

					interval<value_type> c;
					c.start = transition.parameter_set.at(i).first;
					c.end = transition.parameter_set.at(i).second;

					if (cct && pst) {
						interval<value_type> r1 = Intersect(a, b);
						interval<value_type> r2 = Intersect(c, r1);
						transition.parameter_set.at(i).first = r2.start;
						transition.parameter_set.at(i).second = r2.end;
					}
					else {
						to_remove.push_back(tr);
					}

					
				}
				

			}
		}
		tr++;
	}

	for (size_t index = to_remove.size(); index > 0; index--) {
		resulting_transitions.erase(resulting_transitions.begin() + index);
	}

/**/
	m_states.at(state_coord).transitions = resulting_transitions;
	m_states.at(state_coord).processed = true;
	

	return resulting_transitions;
}






//////////////////////////////////////////////////
////////                  Private definitions
//////////////////////////////////////////////////
// base ^ exp
template <typename value_type>
size_t model_automaton<value_type>::ipow(size_t base, size_t exp) {
	assert(exp >= 0);
	int result = 1;
	while (exp)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		base *= base;
	}

	return result;
}

template <typename value_type>
void model_automaton<value_type>::ComputeTransitionsForDimension(size_t dim,
	value_type threshold1, value_type threshold2, std::vector<value_type> variables, std::vector<int> state_coord,
	interval<value_type> &down, interval<value_type> &up, interval<value_type> &self, size_t &p_dim) {
	//DEBUG_METHOD();
/*
	if ((state_coord.at(0) == 4) && (state_coord.at(1) == 6)) {
		DEBUG_MESSAGE("OH FUCK");

	}
	*/
	std::vector<value_type> temp(variables);
	vector<model_transition<value_type>> results_transitions;

	value_type val1(0), val2(0);
	value_type p_val1(0), p_val2(0);
	value_type p1, p2;

	bool isDown = false;
	bool isUp = false;
	bool isSelf = false;

	bool c1 = false;
	bool c2 = false;

	bool bottom_edge = true;
	bool up_edge = true;
	
	for (size_t i = 0; i < variables.size(); i++) {
		if (i != dim) {
			if (variables.at(i) != -INFINITY) {
				bottom_edge = false;
			}
			if (variables.at(i) != INFINITY) {
				up_edge = false;
			}
		}
	}
	if (threshold1 != -INFINITY) {
		bottom_edge = false;
	}
	if (threshold2 != INFINITY) {
		up_edge = false;
	}



	if (bottom_edge) {
		//self.UpdateInterval(-INFINITY, INFINITY);
	}

	if (up_edge) {
		//self.UpdateInterval(-INFINITY, INFINITY);
	}
	

	if (threshold1 != -INFINITY) {
		c1 = true;
	}
	if (threshold2 != INFINITY) {
		c2 = true;
	}
	getValueOfNonparamSubmembersForDim(variables, dim, threshold1, val1, p_val1, p_dim);
	getValueOfNonparamSubmembersForDim(variables, dim, threshold2, val2, p_val2, p_dim);
/*
	DEBUG_VARIABLE(state_coord.at(0));
	DEBUG_VARIABLE(state_coord.at(1));
	DEBUG_VARIABLE(dim);

	DEBUG_VARIABLE(p_dim);

	DEBUG_VARIABLE(variables.at(0));
	DEBUG_VARIABLE(variables.at(1));

	DEBUG_VARIABLE(threshold1);
	DEBUG_VARIABLE(threshold2);

	DEBUG_VARIABLE(val1);
	DEBUG_VARIABLE(val2);

	DEBUG_VARIABLE(p_val1);
	DEBUG_VARIABLE(p_val2);

	DEBUG_VARIABLE(-val1 / p_val1);
	DEBUG_VARIABLE(-val2 / p_val2);
	*/

	if (!isEqual(p_val1, 0)) {
		p1 = -val1 / p_val1;
	}

	if (!isEqual(p_val2, 0)) {
		p2 = -val2 / p_val2;
	}

	// 1
	if ((isEqual(p_val1, 0)) && (isEqual(p_val2,0))) {
		// A
		if ((isBigger(val1, 0)) && (isLess(val2, 0))) {
			self.UpdateInterval(-INFINITY, INFINITY);
		}

		// B
		if ((isLess(val1, 0)) && (isBigger(val2, 0))) {
			if (c2) {
				up.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
			if (c1) {
				down.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
		}

		// C
		if ((isBigger(val1, 0)) && (isBigger(val2, 0))) {
			if (c2) {
				up.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
		}

		// D
		if ((isLess(val1, 0)) && (isLess(val2, 0))) {
			if (c1) {
				down.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
		}

		// E
		if ((isEqual(val1, 0)) || (isEqual(val2, 0))) {
			self.UpdateInterval(-INFINITY, INFINITY);
		}
	}

	// 2
	if ((isEqual(p_val1, 0)) && (isBigger(p_val2, 0))) {
		// A
		if (isBigger(val1, 0)) {
			self.UpdateInterval(-INFINITY, p2);
			if (c2) {
				up.UpdateInterval(p2, INFINITY);
			}
			else {
				self.UpdateInterval(p2, INFINITY);
			}
		}

		// B
		if (isLess(val1, 0)) {
			if (c2) {
				up.UpdateInterval(p2, INFINITY);
			}
			else {
				self.UpdateInterval(p2, INFINITY);
			}
			if (c1) {
				down.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
		}

		// C
		if (isEqual(val1, 0)) {
			self.UpdateInterval(-INFINITY, p2);
			if (c2) {
				up.UpdateInterval(p2, INFINITY);
			}
			else {
				self.UpdateInterval(p2, INFINITY);
			}
		}
	}

	// 3
	if ((isBigger(p_val1, 0)) && (isEqual(p_val2, 0))) {
		// A
		if (isBigger(val2, 0)) {
			if (c2) {
				up.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
			if (c1) {
				down.UpdateInterval(-INFINITY, p1);
			}
			else {
				self.UpdateInterval(-INFINITY, p1);
			}
		}
		
		// B
		if (isLess(val2, 0)) {
			self.UpdateInterval(p1, INFINITY);
			if (c1) {
				down.UpdateInterval(-INFINITY, p1);
			}
			else {
				self.UpdateInterval(-INFINITY, p1);
			}
		}

		// C
		if (isEqual(val2, 0)) {
			self.UpdateInterval(p1, INFINITY);
			if (c1) {
				down.UpdateInterval(-INFINITY, p1);
			}
			else {
				self.UpdateInterval(-INFINITY, p1);
			}
		}
	}

	// 4
	if ((isEqual(p_val1, 0)) && (isLess(p_val2, 0))) {
		// A
		if (isBigger(val1, 0)) {
			self.UpdateInterval(p2, INFINITY);
			if (c2) {
				up.UpdateInterval(-INFINITY, p2);
			}
			else {
				self.UpdateInterval(-INFINITY, p2);
			}
		}

		// B
		if (isLess(val1, 0)) {
			if (c2) {
				up.UpdateInterval(-INFINITY, p2);
			}
			else {
				self.UpdateInterval(-INFINITY, p2);
			}
			if (c1) {
				down.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
		}

		// C
		if (isEqual(val1, 0)) {
			self.UpdateInterval(p2, INFINITY);
			if (c2) {
				up.UpdateInterval(-INFINITY, p2);
			}
			else {
				self.UpdateInterval(-INFINITY, p2);
			}
		}
	}

	// 5
	if ((isLess(p_val1, 0)) && (isEqual(p_val2, 0))) {
		// A
		if (isBigger(val2, 0)) {
			if (c2) {
				up.UpdateInterval(-INFINITY, INFINITY);
			}
			else {
				self.UpdateInterval(-INFINITY, INFINITY);
			}
			if (c1) {
				down.UpdateInterval(p1, INFINITY);
			}
			else {
				self.UpdateInterval(p1, INFINITY);
			}
		}

		// B
		if (isLess(val2, 0)) {
			self.UpdateInterval(-INFINITY, p1);
			if (c1) {
				down.UpdateInterval(p1, INFINITY);
			}
			else {
				self.UpdateInterval(p1, INFINITY);
			}
		}

		// C
		if (isEqual(val2, 0)) {
			self.UpdateInterval(-INFINITY, p1);
		}
	}

	// 6
	if ((isBigger(p_val1, 0)) && (isBigger(p_val2, 0))) {
		if (c2) {
			up.UpdateInterval(min(p1, p2), INFINITY);
		}
		else {
			self.UpdateInterval(min(p1, p2), INFINITY);
		}
		if (c1) {
			down.UpdateInterval(-INFINITY, max(p1, p2));
		}
		else {
			self.UpdateInterval(-INFINITY, max(p1, p2));
		}
	}

	// 7
	if ((isLess(p_val1, 0)) && (isLess(p_val2, 0))) {
		if (c2) {
			up.UpdateInterval(-INFINITY, max(p1, p2));
		}
		else {
			self.UpdateInterval(-INFINITY, max(p1, p2));
		}
		if (c1) {
			down.UpdateInterval(min(p1, p2), INFINITY);
		}
		else {
			self.UpdateInterval(min(p1, p2), INFINITY);
		}
	}

	//8
	if ((isBigger(p_val1, 0)) && (isLess(p_val2, 0))) {
		self.UpdateInterval(max(p1, p2), INFINITY);
		if (c2) {
			up.UpdateInterval(-INFINITY, p2);
		}
		else {
			self.UpdateInterval(-INFINITY, p2);
		}
		if (c1) {
			down.UpdateInterval(-INFINITY, p1);
		}
		else {
			self.UpdateInterval(-INFINITY, p1);
		}
	}

	// 9
	if ((isLess(p_val1, 0)) && (isBigger(p_val2, 0))) {
		self.UpdateInterval(-INFINITY, min(p1, p2));
		if (c2) {
			up.UpdateInterval(p2, INFINITY);
		}
		else {
			self.UpdateInterval(p2, INFINITY);
		}
		if (c1) {
			down.UpdateInterval(p1, INFINITY);
		}
		else {
			self.UpdateInterval(p1, INFINITY);
		}
	}


}

// &val - output value of all summembers except the one with a parameter
// &p_val - output value of the summember with a parameter
// &p_idm - output parameter
template <typename value_type>
void model_automaton<value_type>::getValueOfNonparamSubmembersForDim(std::vector<value_type> variables, size_t dim, value_type threshold, 
	value_type &val, value_type &p_val, size_t &p_dim) const {
	//DEBUG_METHOD();

	val = 0;
	p_val = 0;
	
	std::vector<Summember<value_type>> const & eq = m_model.getEquationForVariable(dim);
	variables.at(dim) = threshold;

	for (std::size_t i = 0; i < eq.size(); ++i)
	{
		value_type v = eq[i].GetConstant();
		
		for (std::size_t j = 0; j < eq[i].GetVars().size(); ++j)
		{
			int temp_variable = eq[i].GetVars().at(j)-1;
				v *= variables.at(temp_variable);
		}

		for (std::size_t j = 0; j < eq[i].GetRamps().size(); ++j) {
			v *= eq[i].GetRamps().at(j).value(variables.at(eq[i].GetRamps().at(j).dim - 1));
		}

		if (eq[i].hasParam())
		{
			p_dim = eq[i].GetParam()-1;
			p_val = v;
		}
		else
		{
			val += v;
		}
	}
}

template <typename value_type>
model_transition<value_type> model_automaton<value_type>::MakeTransition(std::vector<int> original_state,
	size_t dim, value_type parameter_start, value_type parameter_end, size_t p_dim, int up)  {
	//DEBUG_METHOD();

	assert((up >= -1) && (up <= 1));

	model_transition<value_type> new_tr;

	new_tr.parameter_set.resize(m_model.getParamRanges().size());
	size_t d = 0;
	for (auto &i : new_tr.parameter_set) {
		//i.AddInterval(-INFINITY, INFINITY);
		i.first = m_model.getParamRange(d).first;
		i.second = m_model.getParamRange(d).second;
		d++;
	}

	value_type bottom_edge = m_model.getParamRange(p_dim).first;
	value_type top_edge = m_model.getParamRange(p_dim).second;
	if (parameter_start < bottom_edge) {
		parameter_start = bottom_edge;
	}
	if (parameter_end > top_edge) {
		parameter_end = top_edge;
	}

	new_tr.parameter_set.at(p_dim).first = parameter_start;
	new_tr.parameter_set.at(p_dim).second = parameter_end;
	

	new_tr.variable_range.resize(dims);
	d = 0;
	for (auto &i : new_tr.variable_range) {
		if (original_state.at(d) <= 0) {
			i.first = -INFINITY;
		}
		else {
			i.first = m_model.getThresholdsForVariable(d).at(original_state.at(d) - 1);// TODO: m_thresholds?
		}

		if (original_state.at(d) + 1 >= (int)m_model.getThresholdsForVariable(d).size()) {
			i.second = INFINITY;
		}
		else {
			i.second = m_model.getThresholdsForVariable(d).at(original_state.at(d));
		}
		d++;
	}

	new_tr.destinated_state = original_state;

	if (up==1) {
		int new_interval = ++new_tr.destinated_state.at(dim);		

		new_tr.variable_range.at(dim).first = m_model.getThresholdsForVariable(dim).at(new_interval-1);

		if (new_interval >= (int)m_model.getThresholdsForVariable(dim).size()) {
			new_tr.variable_range.at(dim).second = INFINITY;
		}
		else {
			new_tr.variable_range.at(dim).second = m_model.getThresholdsForVariable(dim).at(new_interval);
		}
	}
	if (up==-1) {
		int new_interval = --new_tr.destinated_state.at(dim);

		if (new_interval <= 0) {
			new_tr.variable_range.at(dim).first = -INFINITY;
		}
		else {
			new_tr.variable_range.at(dim).first = m_model.getThresholdsForVariable(dim).at(new_interval - 1);
		}

		new_tr.variable_range.at(dim).second = m_model.getThresholdsForVariable(dim).at(new_interval);
	}

	// make a new state
	// for destinated_state state, current state is a previous state
	typename unordered_map<std::vector<int>, state, hasher>::iterator got = m_states.find(new_tr.destinated_state);
	if (got == m_states.end()) {
		// make a state

		state st;
		st.previous_states.push_back(original_state);
		m_states.emplace(new_tr.destinated_state, st);
	}//TODO: optimize?
	else {
		state &s = got->second;
		s.previous_states.push_back(original_state);
	}

	return new_tr;
}
