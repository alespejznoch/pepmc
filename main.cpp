#include "parser/Parser.h"
#include "data_model/Model.h"
#include "interval_set/interval_set.hpp"

#include "model_automaton.hpp"
#include "property_automaton.hpp"
#include "product_automaton.hpp"

#include "parameter_scanner.hpp"

#include <cstring>
#include <iostream>
#include <iomanip>
#include <deque>
#include <queue>


//#ifdef __gnuc__
//
//#include <sys/time.h>
//
//long long my_clock()
//{
//	timeval tv;
//	gettimeofday(&tv, 0);
//	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
//}
//
//#else
//
//#include <windows.h>
//
//long long my_clock()
//{
//	return gettickcount();
//}
//
//#endif

typedef unsigned int uint;



//class default_pmc_visitor
//{
//public:
//	long long time;
//	bool show_counterexamples;
//	bool show_base_coloring;
//	bool verbose;
//
//	std::size_t reachable_vertices;
//
//	explicit default_pmc_visitor(bool show_counterexamples, bool show_base_coloring, bool verbose)
//		: show_counterexamples(show_counterexamples), show_base_coloring(show_base_coloring), verbose(verbose), reachable_vertices(0)
//	{
//	}
//
//	//shows time between steps
//	void progress(int step, int max)
//	{
//		long long value = 0;
//		if (step == 0)
//		{
//			time = my_clock();
//		}
//		else
//		{
//			long long new_time = my_clock();
//			value = new_time - time;
//			time = new_time;
//		}
//
//		std::cout << step << "/" << max << ": " << value << "ms        \r" << std::flush;
//	}
//
//};

int main(int argc, char * argv[])
{
	std::vector<std::string> varnames;

	struct schedule_entry
	{
		std::string succ_next;
		std::string succ_message;
		std::string fail_next;
		std::string fail_message;
	};

	
	
	// file opening
	char *file_name = argv[argc - 1];
	if (!strncmp(file_name, "bio", 3)) {
		std::cout << "Bio file name error." << std::endl;
	}

	ifstream fin(file_name);	
	ifstream fin2(file_name);

	
	if (!fin || !fin2) {
		std::cout << "Bio file could not be read." << std::endl;
	}
	
	string l;

	Parser parser(fin);
	
	parser.parse();

	Model<double> &storage = parser.returnStorage();

	storage.RunAbstraction();

	fin.close();
	std::vector<std::pair<double, double> > param_ranges(storage.getParamRanges());
	/*
	interval_set<double> space;

	
	double g = 0.0;
	for (int i = 0; i < 300; i++) {
		g += 0.01;
	}
	space.AddInterval(3, 10);
	space.AddInterval(1, 1);
	space.AddInterval(49, 51);
	space.AddInterval(-4, -3);

	space.AddInterval(16, 18);
	space.AddInterval(18.0000001, 19);

	space.AddInterval(56, 80);
	space.AddInterval(60, 80);

	space.AddInterval(-100, 100);


	space.MergeIntervals();
	*/
	
	/*
	model_automaton<double> MA(storage);
	MA.First();

	std::vector<int> destinated_state(3);
	destinated_state.at(0) = 3;
	destinated_state.at(1) = 5;
	destinated_state.at(2) = 6;

	MA.Next(destinated_state);
	MA.Next(destinated_state);
	
	/*
	property_automaton<double> PA(storage);
	PA.initialize(fin2);
	vector<property_transition<double>> test;
	test = PA.First();
	test = PA.Next(1);
	test = PA.Next(1);
	*/
	/*
	interval_set<double> t1;
	t1.AddInterval(10, 16);
	t1.AddInterval(15.8, 32);
	t1.AddInterval(15, 32);
	t1.AddInterval(32, 33.5);
	t1.AddInterval(-2.2, 0);
	t1.AddInterval(130, 5827);
	t1.AddInterval(10000, 10000);
	t1.AddInterval(-8, -7);

	interval_set<double> t2;
	t2.AddInterval(-7,-2.2);
	t2.AddInterval(11,15.9);
	t2.AddInterval(10000,10000);
	t2.AddInterval(4000,5101);
	t2.AddInterval(-20,-7.5);
	t2.AddInterval(-1,7);

	interval_set<double> r = IntersectIntervals(t2, t1);

	bool borec;
	bool bycek;
	bool bzuk;

	borec = t1.isSubSetOf(t2);
	bycek = r.isSubSetOf(t1);
	bzuk = r.isSubSetOf(t2);

	interval_set<double> i1;
	t1.AddInterval(10, 16);
	i1.AddInterval(-INFINITY, INFINITY);

	interval_set<double> i2;
	i2.AddInterval(-INFINITY, INFINITY);

	interval_set<double> TEST = i2.DifferenceWith(i1);
	*/
	

	interval_set<double> t1;
	t1.AddInterval(4, 6);
	t1.AddInterval(-4, 1);

	interval_set<double> t2;
	t2.AddInterval(-INFINITY, 3);

	bool prafda = true;
	prafda = t1.isSubSetOf(t2);
	



	product_automaton<double> ba(storage);
	ba.initialize(fin2);


	bool show_counterexamples = false;
	bool show_base_coloring = false;
	bool verbose = false;


	// Parse arguments
	for (int i = 1; i < argc; ++i)
	{
		std::string arg = argv[i];

		if (arg == "-c")
			show_counterexamples = true;

		if (arg == "-b")
			show_base_coloring = true;

		if (arg == "-v")
			verbose = true;

		/*if (arg == "-t")
		{
			std::cout << "thresholds:" << std::endl;
			for (std::size_t i = 0; i < ss2.m_thresholds.size(); ++i)
			{
				std::cout << storage.getVariable(i) << ":";
				for (std::size_t j = 0; j < ss2.m_thresholds[i].size(); ++j)
				{
					if (j > 0)
						std::cout << ',';
					std::cout << ss2.m_thresholds[i][j];
				}
				std::cout << std::endl;
			}
		}*/

		if (arg == "parse")
			return 0;


	}

	parameter_scanner<double> ps(ba);
	paramset<double> result = ps.Scan();
	DEBUG_MESSAGE(result);

	/*
	std::deque<std::pair<std::string, pp_t> > sch_queue;
	sch_queue.push_back(std::make_pair("0", std::move(pp3)));

	for (; !sch_queue.empty(); sch_queue.pop_front())
	{

		schedule_entry const & sch = schedule[sch_queue.front().first];

		std::cout << "# " << sch_queue.front().first << ": " << sch_queue.front().second << std::endl;


		
		sba_t sba(bas[sch_queue.front().first], ss3);
		
		default_pmc_visitor visitor(show_counterexamples, show_base_coloring, verbose);
		
		succ sucak(thread_count);
		
		
		//sch_queue.front().second - states, intervaly parametru
		//sba - sync product
		//violating_parameters - novej paramset
		
		pp_t violating_parameters;
		
		if (verbose)
			std::cout << "reachable vertices: " << visitor.reachable_vertices << std::endl;
		
		pp_t correct_parameters = sch_queue.front().second;
		correct_parameters.set_difference(violating_parameters);
		
		std::cout << sch.succ_message << "true set : " << sch.succ_next << ": " << correct_parameters << std::endl;
		if (!sch.succ_next.empty())
			sch_queue.push_back(std::make_pair(sch.succ_next, std::move(correct_parameters)));
		
		std::cout << sch.fail_message << "refuted set: " << sch.fail_next << ": " << violating_parameters << std::endl;
		if (!sch.fail_next.empty())
			sch_queue.push_back(std::make_pair(sch.fail_next, std::move(violating_parameters)));

		std::cout << std::endl;
	}
	*/
	
}
