#include "product_automaton.hpp"

template <typename value_type>
void product_automaton<value_type>::initialize(std::istream &in) {
	DEBUG_METHOD();

	m_pa.initialize(in);
}

template <typename value_type>
state<value_type> product_automaton<value_type>::getState(key k) {
	state<value_type> st = m_states.at(k);
	return st;
}

template <typename value_type>
vector<transition<value_type>> product_automaton<value_type>::First() {
	DEBUG_METHOD();

	vector<transition<value_type>> result_transitions;
	
	std::vector<model_transition<value_type>> model_transitions = m_ma.First();
	std::vector<property_transition<value_type>> property_transitions = m_pa.First();

	result_transitions = ComputeTransitions(model_transitions, property_transitions);

	return result_transitions;
}

template <typename value_type>
vector<transition<value_type>> product_automaton<value_type>::Next(key key_state) {
	//DEBUG_METHOD();
	//DEBUG_MESSAGE(key_state);

	vector<transition<value_type>> result_transitions;

	//if ((key_state.model_state.at(0) = 4) && (key_state.model_state.at(1) = 5)) {
	//	DEBUG_MESSAGE(key_state);
	//}

	typename unordered_map<key, state<value_type>, hasher>::const_iterator got = m_states.find(key_state);
	if (got == m_states.end()) {
	// make a state

	state<value_type> st;
	m_states.emplace(key_state, st);
	}
	else if (true == m_states.at(key_state).processed) {
		return (m_states.at(key_state).transitions);
	}

	
	if (m_pa.isFinal(key_state.property_state) == true) {
		m_states.at(key_state).final = true;
	}
	

	std::vector<model_transition<value_type>> model_transitions = m_ma.Next(key_state.model_state);
	std::vector<property_transition<value_type>> property_transitions = m_pa.Next(key_state.property_state);

	result_transitions = ComputeTransitions(model_transitions, property_transitions);

	m_states.at(key_state).transitions = result_transitions;
	m_states.at(key_state).processed = true;

	return result_transitions;
}


//////////////////////////////////////////////////
////////                  Private definitions
//////////////////////////////////////////////////

template <typename value_type>
vector<transition<value_type>> product_automaton<value_type>::ComputeTransitions(vmt model_transitions, vpt property_transitions) {
	//DEBUG_METHOD();

	vector<transition<value_type>> result_transitions;

	for (auto model_transition : model_transitions) {
		for (auto property_transition : property_transitions) {		
			transition<value_type> result_transition;
			if (true == MakeTransition(model_transition, property_transition, result_transition)) {
				result_transitions.push_back(result_transition);
			}
		}
	}

	return result_transitions;
}

template <typename value_type>
bool product_automaton<value_type>::MakeTransition(
	model_transition<value_type> model_transition, property_transition<value_type> property_transition,
	transition<value_type> &result_transition) {
	//DEBUG_METHOD();
	
	//was commented
	//@todo
	std::vector<interval<value_type>> &variable_space = result_transition.variable_space;

	
	for (size_t i = 0; i < dims; i++) {
		value_type ia_start = model_transition.variable_range.at(i).first;
		value_type ia_end = model_transition.variable_range.at(i).second;
		value_type ib_start = property_transition.variable_range.at(i).first;
		value_type ib_end = property_transition.variable_range.at(i).second;
		interval<value_type> result_interval;

		interval_set<value_type> model_range;
		model_range.AddInterval(model_transition.variable_range.at(i).first, model_transition.variable_range.at(i).second);
		interval_set<value_type> property_range;
		property_range.AddInterval(property_transition.variable_range.at(i).first, property_transition.variable_range.at(i).second);

		if (!model_range.isSubSetOf(property_range)) {
			return false;
		}
		///*
		// ia is too left to intersect
		if (isLess(ia_end, ib_start)) {
			return false;
		}

		// ib is too left to intersect
		if (isLess(ib_end, ia_start)) {
			return false;
		}

		// Testing of four different possibilities
		if (isLess(ia_start, ib_start)) {
			if (isLess(ib_end, ia_end)) {
				result_interval.setInterval(ib_start, ib_end);
				variable_space.push_back(result_interval);
				continue;
			}
			else {
				result_interval.setInterval(ib_start, ia_end);
				variable_space.push_back(result_interval);
				continue;
			}
		}
		else {
			if (isLess(ib_end, ia_end)) {
				result_interval.setInterval(ia_start, ib_end);
				variable_space.push_back(result_interval);
				continue;
			}
			else {
				result_interval.setInterval(ia_start, ia_end);
				variable_space.push_back(result_interval);
				continue;
			}
		}
		//*/
	}

	for (size_t i = 0; i < m_model.getParamRanges().size(); i++) {
		value_type start = model_transition.parameter_set.at(i).first;
		value_type end = model_transition.parameter_set.at(i).second;
		interval<value_type> result_interval;
		result_interval.setInterval(start, end);
		result_transition.parameter_space.push_back(result_interval);
	}

	key destinated_state;
	destinated_state.model_state = model_transition.destinated_state;
	destinated_state.property_state = property_transition.id2;
	result_transition.destinated_state = destinated_state;

	return true;
}

template <typename value_type>
std::vector<interval_set<value_type>> product_automaton<value_type>::getParamset() {
	std::vector<interval_set<value_type>> result;

	result.resize(m_model.getParamRanges().size());
	size_t d = 0;
	for (auto &r : result) {
		r.AddInterval(m_model.getParamRange(d).first, m_model.getParamRange(d).second);
		d++;
	}

	return result;
}
