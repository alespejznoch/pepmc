#include "interval_set.hpp"

///@todo optimize
template <typename value_type>
interval_set<value_type> IntersectIntervals(interval_set<value_type> &A, interval_set<value_type> &B) {
	//DEBUG_METHOD();
	A.MergeIntervals();
	B.MergeIntervals();
	interval_set<value_type> result_set;

	size_t it_a(0), it_b(0);
	while ((it_a < A.GetIntervals().size()) && (it_b < B.GetIntervals().size())) {
		interval<value_type> ia = A.GetIntervals().at(it_a);
		interval<value_type> ib = B.GetIntervals().at(it_b);

		// ia is too left to intersect
		if (isLess(ia.end, ib.start)) {
			++it_a;
			continue;
		}

		// ib is too left to intersect
		if (isLess(ib.end, ia.start)) {
			++it_b;
			continue;
		}

		// Testing of four different possibilities
		if (isLess(ia.start, ib.start)) {
			if (isLess(ib.end, ia.end)) {
				result_set.AddInterval(ib.start, ib.end);
				++it_b;
				continue;
			}
			else {
				result_set.AddInterval(ib.start, ia.end);
				++it_a;
				continue;
			}
		}
		else {
			if (isLess(ib.end, ia.end)) {
				result_set.AddInterval(ia.start, ib.end);
				++it_b;
				continue;
			}
			else {
				result_set.AddInterval(ia.start, ia.end);
				++it_a;
				continue;
			}
		}

	}

	return result_set;
}


template <typename value_type>
void interval_set<value_type>::MergeIntervals() {
	//DEBUG_METHOD();

	if (merged) {
		return;
	}
	merged = 1;
	if (intervals.empty()) {
		return;
	}

	stack<interval<value_type>> s;
	vector<interval<value_type>> result;

	// Sort the intervals based on start time
	sort(intervals.begin(), intervals.end(), *this);

	// Push the first interval to stack
	s.push(intervals[0]);

	// Start from the next interval and merge if necessary
	for (int i = 1; i < (int)intervals.size(); i++)
	{
		interval<value_type> top = s.top();

		// If current interval is not overlapping with stack top,
		// push it to the stack
		if (isLess(top.end, intervals[i].start))
		{
			s.push(intervals[i]);
			result.push_back(top);
		}
		// Else update the ending time of top if ending of current 
		// interval is more
		else if (isLess(top.end, intervals[i].end))
		{
			top.end = intervals[i].end;
			s.pop();
			s.push(top);
		}		
	}

	result.push_back(s.top());
	intervals.clear();
	intervals = result;

	return;
}

template <typename value_type>
void interval_set<value_type>::AddInterval(value_type l, value_type r) {
	//DEBUG_METHOD();

	merged = 0;

	if (!isLess(l, r)) { return; }; //TODO?

	interval<value_type> new_interval;
	new_interval.setInterval(l, r);

	intervals.push_back(new_interval);
}

template <typename value_type>
void interval_set<value_type>::AddInterval(interval<value_type> new_interval) {
	//DEBUG_METHOD();

	merged = 0;
	intervals.push_back(new_interval);
}

template <typename value_type>
vector<interval<value_type>> interval_set<value_type>::GetIntervals() {
	//DEBUG_METHOD();

	if (!merged) {
		MergeIntervals();
	}
	
	return intervals;
}


template <typename value_type>
bool interval_set<value_type>::isSubSetOf(interval_set<value_type> &B) {
	//DEBUG_METHOD();
	// TODO: empty vectors
	this->MergeIntervals();
	B.MergeIntervals();
	interval_set<value_type> result_set;
	interval_set<value_type> intersection =	IntersectIntervals(*this, B);

	size_t it_a(0), it_b(0);
	if (intersection.intervals.size() != (this)->intervals.size()) {
		return false;
	}

	for (size_t i = 0; i < intersection.intervals.size(); i++) {
		value_type i1;
		value_type i2;
		intersection.intervals.at(i).getInterval(i1, i2);

		value_type a1;
		value_type a2;
		(this->intervals).at(i).getInterval(a1, a2);

		if ((i1 != a1) || (i2 != a2)) {
			return false;
		}
	}

	return true;
}

template <typename value_type>
interval_set<value_type> interval_set<value_type>::UnionWith(interval_set<value_type> B) {
	//DEBUG_METHOD();

	this->MergeIntervals();
	B.MergeIntervals();
	interval_set<value_type> result_set;

	for (auto i : this->intervals) {
		result_set.AddInterval(i);
	}

	for (auto i : B.intervals) {
		result_set.AddInterval(i);
	}
	
	return result_set;
}

template <typename value_type>
interval_set<value_type> interval_set<value_type>::Complement() {
	//DEBUG_METHOD();

	interval_set<value_type> result_set;

	if (0 == (this->intervals).size()) {
		result_set.AddInterval(-INFINITY, INFINITY);
		return result_set;
	} else if (1 == (this->intervals).size()) {
		value_type s(0), e(0);
		this->intervals.at(0).getInterval(s, e);
		if ((-INFINITY == s) && (INFINITY == e)) {
			return result_set;
		}
	}

	this->MergeIntervals();
	
	value_type token(-INFINITY);
	value_type s(0), e(0);
	for (auto i : this->intervals) {
		i.getInterval(s, e);
		if (-INFINITY == s) {
			token = e;
			continue;
		}

		result_set.AddInterval(token, s);
		token = e;
	}

	// last one
	if (INFINITY != e) {
		result_set.AddInterval(e, INFINITY);
	}


	return result_set;
}

template <typename value_type>
interval_set<value_type> interval_set<value_type>::DifferenceWith(interval_set<value_type> B) {
	//DEBUG_METHOD();

	interval_set<value_type> result_set;
	result_set = B.Complement();
	result_set = IntersectIntervals((*this), result_set);
	return result_set;
}

//////////////////////////////////////////////////
////////                  Private definitions
//////////////////////////////////////////////////
/*
template <typename value_type>
bool interval_set<value_type>::CompareIntervals(const interval<value_type>> &, const interval<value_type> &i2) {
return (isLess(i1.start, i2.start) ? true : false);
}

template <typename value_type>
bool interval_set<value_type>::CompareIntervals(const interval<value_type> &i1, const interval<value_type> &i2) {
return (isLess(i1.start, i2.start) ? true : false);
}
*/
