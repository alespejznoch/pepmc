#include "interval.hpp"

template <typename value_type>
bool interval<value_type>::getInterval(value_type &returned_start, value_type &returned_end) const {
	returned_start = start;
	returned_end = end;

	return empty;
}

template <typename value_type>
void interval<value_type>::setInterval(value_type &new_start, value_type &new_end) {
	start = new_start;
	end = new_end;
	set = true;
	empty = false;
}

template <typename value_type>
void interval<value_type>::setEmpty() {
	set = true;
	empty = true;
}

template <typename value_type>
void interval<value_type>::UpdateInterval(value_type possible_start, value_type possible_end) {
	if (set) {
		if (possible_start < start) {
			start = possible_start;
		}

		if (possible_end > end) {
			end = possible_end;
		}
	}
	else {
		setInterval(possible_start, possible_end);
	}

	empty = false;
}

template <typename value_type>
bool interval<value_type>::isSet() {
	return set;
}

template <typename value_type>
interval<value_type> Intersect(interval<value_type> ia, interval<value_type> ib) {
	interval<value_type> res;

	// Testing of four different possibilities
	if (isLess(ia.start, ib.start)) {
		if (isLess(ib.end, ia.end)) {
			res.setInterval(ib.start, ib.end);
		}
		else {
			res.setInterval(ib.start, ia.end);
		}
	}
	else {
		if (isLess(ib.end, ia.end)) {
			res.setInterval(ia.start, ib.end);
		}
		else {
			res.setInterval(ia.start, ia.end);
		}
	}


	return res;
}
