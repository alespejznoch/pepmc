#ifndef INTERVAL_SET_HPP
#define INTERVAL_SET_HPP

#include "dbg_msg/dbg_msg.hpp"
#include "almost_equal/almost_equal.hpp"
#include "interval.hpp"

#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

template <typename value_type>
class interval_set {

private:

	std::vector<interval<value_type>> intervals;
	bool merged = 0;

	//bool CompareIntervals(const interval<value_type> &i1, const interval<value_type> &i2);

public:
	// Functor
	bool operator()(const interval<value_type> i1, const interval<value_type> i2) {
		return (isLess(i1.start, i2.start) ? true : false);
	}

	void MergeIntervals();
	void AddInterval(value_type l, value_type r);
	void AddInterval(interval<value_type> new_interval);

	bool isSubSetOf(interval_set<value_type> &B);
	interval_set<value_type> UnionWith(interval_set<value_type> B);
	interval_set<value_type> Complement();
	interval_set<value_type> DifferenceWith(interval_set<value_type> B);


	friend std::ostream& operator<<(std::ostream& out, const interval_set<value_type>& t) {
		for (auto &i : t.intervals) {
			out << "(" << i.start << "," << i.end << ") ";
		}
		return out;
	}



	void GetEdges(value_type &first, value_type &last);

	std::vector<interval<value_type>> GetIntervals();
	

};


// Long double is not allowed
template <>
class interval_set < long double > ;

///@todo optimize
template <typename value_type>
interval_set<value_type> IntersectIntervals(interval_set<value_type> &A, interval_set<value_type> &B);

#include "interval_set.tpp"


#endif
