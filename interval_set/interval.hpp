#ifndef INTERVAL_HPP
#define INTERVAL_HPP

#include "almost_equal/almost_equal.hpp"

template <typename value_type>
class interval {

public:
	interval(){}// : start(NULL), end(NULL) {}
	interval(value_type s, value_type e) : start(s), end(e) {}

	bool getInterval(value_type &returned_start, value_type &returned_end) const;
	
	void setInterval(value_type &new_start, value_type &new_end);
	void setEmpty();

	void UpdateInterval(value_type possible_start, value_type possible_end);

	bool isSet();

	value_type start;
	value_type end;
private:
	bool set = 0;
	bool empty = 0;
};

template <typename value_type>
interval<value_type> Intersect(interval<value_type> ia, interval<value_type> ib);

#include "interval.tpp"

#endif
