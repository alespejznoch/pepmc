#ifndef ALMOST_EQUAL_HPP
#define ALMOST_EQUAL_HPP

#include <cfloat>
#include <cmath>
#include <cstdint>
#include <cstdlib>

#define FLOAT_MAX_ULPS_DIFF 10  // Used in isEqual function
#define DOUBLE_MAX_ULPS_DIFF 100  // Used in isEqual function

union float_union {
	float_union(float num = 0.0f) : f(num) {}
	bool Negative() const { return (i >> 31) != 0; }

	float f;
	int32_t i;
};

union double_union {
	double_union(double num = 0.0) : f(num) {}
	bool Negative() const { return (i >> 63) != 0; }

	double f;
	int64_t i;
};

template <typename value_type>
bool isEqual(const value_type &A, const value_type &B);

template <typename value_type>
bool isEqual(const value_type &A, const int &B);

template <typename value_type>
bool isEqual(const int &A, const value_type &B);

inline bool isEqual(const float &A, const float &B);



template <typename value_type>
bool isLess(const value_type &less, const  value_type &more);

template <typename value_type>
bool isLess(const value_type &less, const int &more);

template <typename value_type>
bool isLess(const int &less, const value_type &more);

inline bool isEqual(const double &A, const double &B);


template <typename value_type>
bool isBigger(const value_type &more, const int &less);

template <typename value_type>
bool isBigger(const int &more, const value_type &less);



template <typename value_type>///@todo check ternary change
bool isLessOrEqual(const value_type &less, const  value_type &more);

template <typename value_type>
bool isLessOrEqual(const value_type &less, const int &more);

template <typename value_type>
bool isLessOrEqual(const int &less, const value_type &more);

#include "almost_equal.tpp"

#endif
