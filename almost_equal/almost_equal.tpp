#include "almost_equal.hpp"

template <typename value_type>
bool isEqual(const value_type &A, const value_type &B) {

	return (A == B);
}

template <typename value_type>
bool isEqual(const value_type &A, const int &B) {
	return isEqual(A, (value_type)B);
}
template <typename value_type>
bool isEqual(const int &A, const value_type &B) {
	return isEqual((value_type)A, B);
}


template <typename value_type>
bool isLess(const value_type &less, const  value_type &more) {

	bool equal = isEqual(less, more);

	if (!equal && less < more) {
		return true;
	}

	return false;
}

template <typename value_type>
bool isLess(const value_type &less, const int &more) {
	return isLess(less, (value_type)more);
}
template <typename value_type>
bool isLess(const int &less, const value_type &more) {
	return isLess((value_type)less, more);
}

template <typename value_type>
bool isBigger(const value_type &more, const int &less) {
	return isLess(less, (value_type)more);
}
template <typename value_type>
bool isBigger(const int &more, const value_type &less) {
	return isLess((value_type)less, more);
}


template <typename value_type>///@todo check ternary change
bool isLessOrEqual(const value_type &less, const  value_type &more) {
	
	return !isLess(more, less) ? true : false;
}

template <typename value_type>
bool isLessOrEqual(const value_type &less, const int &more) {
	return isLessOrEqual(less, (value_type)more);
}
template <typename value_type>
bool isLessOrEqual(const int &less, const value_type &more) {
	return isLessOrEqual((value_type)less, more);
}


inline bool isEqual(const float &A, const float &B) {

	float max_diff = FLT_EPSILON;

	// Absolute difference -- needed for comparison of numbers near zero
	float abs_diff = fabs(A - B);
	if (abs_diff <= max_diff) {
		return true;
	}

	float_union union_a(A);
	float_union union_b(B);

	// Different signs means they do not match.
	if (union_a.Negative() != union_b.Negative()) {
		return false;
	}

	int max_ulps = FLOAT_MAX_ULPS_DIFF;

	// Find the difference in ULPs.
	int32_t ulps_diff = abs(union_a.i - union_b.i);
	if (ulps_diff <= max_ulps) {
		return true;
	}

	return false;
}

inline bool isEqual(const double &A, const double &B) {

	double max_diff = DBL_EPSILON;

	// Absolute difference -- needed for comparison of numbers near zero
	double abs_diff = fabs(A - B);
	if (abs_diff <= max_diff) {
		return true;
	}

	double_union union_a(A);
	double_union union_b(B);

	// Different signs means they do not match.
	if (union_a.Negative() != union_b.Negative()) {
		return false;
	}

	int max_ulps = DOUBLE_MAX_ULPS_DIFF;

	// Find the difference in ULPs.
	int64_t ulps_diff = abs(union_a.i - union_b.i);
	if (ulps_diff <= max_ulps) {
		return true;
	}

	return false;
}
