#ifndef PARAMETER_SCANNER
#define PARAMETER_SCANNER

#include "paramset.hpp"

#include "model_automaton.hpp"
#include "property_automaton.hpp"
#include "product_automaton.hpp"

#include <vector>
#include <unordered_map>
#include <cassert>
#include <deque>

template <typename value_type>
class parameter_scanner {
	typedef product_automaton<value_type> ba_type;

public:
	parameter_scanner(ba_type const & ba)
		: m_ba(ba) {
	}

	paramset<value_type> Scan();

private:

	class hasher	{
	public:
		std::size_t operator()(key const& k) const {
			// 0x9e3779b9 - 32 "random" bits
			std::size_t seed = 0;
			for (auto &i : k.model_state) {
				seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
			}
			seed ^= k.property_state + 0x9e3779b9 + (seed << 6) + (seed >> 2);

			return seed;
		}
	};

	template <typename T>
	using colouring = unordered_map<key, paramset<T>, hasher>;

	colouring<value_type> Succ(std::vector<key> keys, paramset<value_type> initial_paramset);
	colouring<value_type> Succ(key k, paramset<value_type> initial_paramset);
	parameter_scanner<value_type>::colouring<value_type> ComputeSucc(deque<std::pair<key, paramset<value_type>>> &Q_new);

	//unordered_map<key, state<value_type>, hasher> m_states;
	product_automaton<value_type> m_ba;
};

// Long double is not allowed
template <>
class parameter_scanner < long double >;

#include "parameter_scanner.tpp"

#endif
