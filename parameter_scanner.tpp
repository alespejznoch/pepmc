#include "parameter_scanner.hpp"

template <typename value_type>
paramset<value_type> parameter_scanner<value_type>::Scan() {
	DEBUG_METHOD();

	paramset<value_type> result;

	paramset<value_type> initial_paramset;
	initial_paramset.parameter_space = m_ba.getParamset();

	result.parameter_space.resize(initial_paramset.parameter_space.size());

	parameter_scanner<value_type>::colouring<value_type> c;
	std::vector<transition<value_type>> ts;
	std::vector<key> first_keys;
	ts = m_ba.First();

	/////////////////////TEST TEST
	/*
	key ss;
	ss.model_state.push_back(0);
	ss.property_state = 0;
	c = Succ(ss, initial_paramset);
	*/

	/////////////////TEST TEST TEST

	for (auto ft : ts) {
		first_keys.push_back(ft.destinated_state);
	}
	
	c = Succ(first_keys, initial_paramset);
	DEBUG_MESSAGE("Succ all FINAL states:");
	for (auto i : c) {
		if (m_ba.getState(i.first).final) {
			paramset<value_type> temp = i.second.DifferenceWith(result);
			if (!temp.isEmpty()) {
				parameter_scanner<value_type>::colouring<value_type> temp_c;
				DEBUG_MESSAGE("Succ from FINAL state: " << i.first);
				temp_c = Succ(i.first, temp);
				if (temp_c.find(i.first) != temp_c.end()) {
					paramset<value_type> res = temp_c.at(i.first);
					result = result.UnionWith(res);
				}


			}
		}
	}

	return result;
}

template <typename value_type>
parameter_scanner<value_type>::colouring<value_type> parameter_scanner<value_type>::Succ(vector<key> keys, 
	paramset<value_type> initial_paramset) {
	DEBUG_METHOD();

	parameter_scanner<value_type>::colouring<value_type> c;
	deque<std::pair<key, paramset<value_type>>> Q_new;

	for (auto i : keys) {
		Q_new.emplace_back(i, initial_paramset);
	}
	c = ComputeSucc(Q_new);

	return c;
}

template <typename value_type>
parameter_scanner<value_type>::colouring<value_type> parameter_scanner<value_type>::Succ(key k, 
	paramset<value_type> initial_paramset) {
	DEBUG_METHOD();

	parameter_scanner<value_type>::colouring<value_type> c;
	deque<std::pair<key, paramset<value_type>>> Q_new;

	std::vector<transition<value_type>> ts;
	std::vector<key> first_keys;
	ts = m_ba.Next(k);
//	if (k.model_state.at(0) == 0) {
//		DEBUG_MESSAGE("mor");
//	}

	for (auto ft : ts) {
		paramset<value_type> temp;
		temp.set(ft.parameter_space);
		temp = temp.IntersectionWith(initial_paramset);
		if (!temp.isEmpty()) {
			Q_new.emplace_back(ft.destinated_state, temp);
		}
	}

	c = ComputeSucc(Q_new);

	return c;
}


template <typename value_type>
parameter_scanner<value_type>::colouring<value_type> parameter_scanner<value_type>::ComputeSucc(deque<std::pair<key, paramset<value_type>>> &Q_new) {
	DEBUG_METHOD();

	parameter_scanner<value_type>::colouring<value_type> c;

	deque<std::pair<key, paramset<value_type>>> Q_old;
	///@todo write a lot of comments
	//TODO: optimize later
	while (!Q_new.empty()) {
		Q_old = Q_new;
		Q_new.clear();
		//DEBUG_MESSAGE("Q_old = Q_new");
		while (!Q_old.empty()) {

			key temp_k = Q_old.front().first;
			paramset<value_type> temp_paramset = Q_old.front().second;
			Q_old.pop_front();

			bool isSubSet = false;
			if (c.find(temp_k) == c.end()) {
				c.emplace(temp_k, temp_paramset);
				isSubSet = true;
			}
			else if (!temp_paramset.isSubSetOf(c.at(temp_k))) {
				c.at(temp_k) = c.at(temp_k).UnionWith(temp_paramset);
				isSubSet = true;
			}

			if (isSubSet) {
				DEBUG_MESSAGE("Start state: " << temp_k << " is: " << c.at(temp_k));
				
				parameter_scanner<value_type>::colouring<value_type> c_tran;

				std::vector<transition<value_type>> temp_transitions = m_ba.Next(temp_k);
				for (auto transition : temp_transitions) {
					paramset<value_type> new_paramset;
					new_paramset.set(transition.parameter_space);
					new_paramset = new_paramset.IntersectionWith(temp_paramset);
					key new_key = transition.destinated_state;
					if (!new_paramset.isEmpty()) {
						c_tran.emplace(new_key, new_paramset);
						DEBUG_MESSAGE("    ---> Final state: " << new_key << " " << new_paramset);
					}
					//DEBUG_MESSAGE("SUCK!");///@todo remove this shit
				}

				for (auto &i : Q_new) {
					key _temp_k = i.first;
					paramset<value_type> _temp_paramset = i.second;

					if (c_tran.find(_temp_k) == c_tran.end()) {
						continue;
					}
					paramset<value_type> _temp_paramset2 = c_tran.at(_temp_k);
					c_tran.erase(_temp_k);
					i.second = _temp_paramset.UnionWith(_temp_paramset2);
				}

				for (auto i : c_tran) {
					key _temp_k = i.first;
					paramset<value_type> _temp_paramset = i.second;
					Q_new.emplace_back(_temp_k, _temp_paramset);
				}
				/*
				DEBUG_MESSAGE("Q_new:");
				for (auto i : Q_new) {
					DEBUG_MESSAGE("  " << i.first << " " << i.second);
				}
				*/

			}


		}
	}

	return c;
}
