#ifndef PRODUCT_AUTOMATON_HPP
#define PRODUCT_AUTOMATON_HPP

#include "dbg_msg/dbg_msg.hpp"
#include "interval_set/interval_set.hpp"

#include "model_automaton.hpp"
#include "property_automaton.hpp"

#include "data_model/Model.h"

#include <string>
#include <vector>
#include <unordered_map>

struct key {
	std::vector<int> model_state;
	size_t property_state;
	//size_t token;

	friend bool operator == (const key &k1, const key &k2)	{
		return (k1.model_state == k2.model_state &&	k1.property_state == k2.property_state);
	}

//	friend std::ostream& operator<<(std::ostream& out, const key& t) {
//		out << "Model State: ( ";
//		for (auto &i : t.model_state) {
//			out << i << " ";
//		}
//		out << ")  Property State: (" << t.property_state << ")";
//		return out;
//	}
	
	friend std::ostream& operator<<(std::ostream& out, const key& t) {
		out << "(";
		for (uint i=0; i<t.model_state.size(); ++i) {
			if (i!=0) {
				out << ',';
			}
			out << t.model_state.at(i);
		}
		out << ")x(" << t.property_state << ")";
		return out;
	}
};

template <typename value_type>
struct transition {
	//@todo
	std::vector<interval<value_type>> variable_space;
	std::vector<interval<value_type>> parameter_space;

	key destinated_state;
};

template <typename value_type>
struct state {
	std::vector<transition<value_type>> transitions;
	bool final = false;
	bool processed = false;
};


template <typename value_type>
class product_automaton {
	typedef Model<value_type> model_type;
	typedef std::vector<model_transition<value_type>> vmt;
	typedef std::vector<property_transition<value_type>> vpt;

public:
	product_automaton(model_type const & model)
		: m_model(model), dims(m_model.getDims()), m_ma(model), m_pa(model) {		
	}

	void initialize(std::istream &in);

	state<value_type> getState(key k);

	vector<transition<value_type>> First();
	vector<transition<value_type>> Next(key state);

	std::vector<interval_set<value_type>> getParamset();

private:

	vector<transition<value_type>> ComputeTransitions(vmt model_transitions, vpt property_transitions);

	bool MakeTransition(model_transition<value_type> model_transition, property_transition<value_type> property_transition,
		transition<value_type> &result_transition);

	class hasher	{
	public:
		std::size_t operator()(key const& k) const {
			// 0x9e3779b9 - 32 "random" bits
			std::size_t seed = 0;
			for (auto &i : k.model_state) {
				seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2); 
			}
			seed ^= k.property_state + 0x9e3779b9 + (seed << 6) + (seed >> 2);

			return seed;
		}
	};

	model_automaton<value_type> m_ma;
	property_automaton<value_type> m_pa;
	model_type m_model;
	size_t dims;

	unordered_map<key, state<value_type>, hasher> m_states;
};

// Long double is not allowed
template <>
class product_automaton < long double >;


#include "product_automaton.tpp"

#endif
