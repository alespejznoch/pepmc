#ifndef PARAMSET_HPP
#define PARAMSET_HPP

#include "dbg_msg/dbg_msg.hpp"
#include "almost_equal/almost_equal.hpp"
#include "interval_set/interval_set.hpp"

template <typename value_type>
struct paramset {
	std::vector<interval_set<value_type>> parameter_space;

	bool isSubSetOf(paramset<value_type> B);
	paramset<value_type> UnionWith(paramset<value_type> B);
	paramset<value_type> IntersectionWith(paramset<value_type> B);
	paramset<value_type> DifferenceWith(paramset<value_type> B);

	void set(const std::vector<interval<value_type>> p);
	bool isEmpty();

	friend bool operator == (const paramset &p1, const paramset &p2)	{
		return (p1.parameter_space == p2.parameter_space);
	}

	paramset<value_type>& operator = (const paramset<value_type> &B){
		parameter_space = B.parameter_space;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream& out, const paramset<value_type>& t) {
		out << std::endl;
		debug::WriteIndentation(); 
		out << "       P: [ ";
		for (auto &i : t.parameter_space) {
			out << i << "]" << std::endl;
		}
		return out;
	}

};

#include "paramset.tpp"

#endif
