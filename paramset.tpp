#include "paramset.hpp"

template <typename value_type>
bool paramset<value_type>::isSubSetOf(paramset<value_type> B) {
	bool res = true;

	assert(this->parameter_space.size() == B.parameter_space.size());
	
	for (size_t i = 0; i < B.parameter_space.size(); i++) {
		if (!this->parameter_space.at(i).isSubSetOf(B.parameter_space.at(i))) {
			res = false;
		}
	}

	return res;
}

template <typename value_type>
paramset<value_type> paramset<value_type>::UnionWith(paramset<value_type> B) {

	assert(this->parameter_space.size() == B.parameter_space.size());

	paramset<value_type> res;
	res.parameter_space.resize(parameter_space.size());

	for (size_t i = 0; i < B.parameter_space.size(); i++) {
		res.parameter_space.at(i) = (this->parameter_space.at(i)).UnionWith(B.parameter_space.at(i));
	}

	return res;
}

template <typename value_type>
paramset<value_type> paramset<value_type>::IntersectionWith(paramset<value_type> B) {

	assert(this->parameter_space.size() == B.parameter_space.size());

	paramset<value_type> res;
	res.parameter_space.resize(parameter_space.size());

	for (size_t i = 0; i < B.parameter_space.size(); i++) {
		res.parameter_space.at(i) = IntersectIntervals(this->parameter_space.at(i), B.parameter_space.at(i));
	}

	return res;
}

template <typename value_type>
paramset<value_type> paramset<value_type>::DifferenceWith(paramset<value_type> B) {

	assert(this->parameter_space.size() == B.parameter_space.size());

	paramset<value_type> res;
	res.parameter_space.resize(parameter_space.size());

	for (size_t i = 0; i < B.parameter_space.size(); i++) {
		res.parameter_space.at(i) = (this->parameter_space.at(i)).DifferenceWith(B.parameter_space.at(i));
	}

	return res;
}

template <typename value_type>
void paramset<value_type>::set(const std::vector<interval<value_type>> p) {

	parameter_space.clear();
	parameter_space.resize(p.size());
	for (size_t i = 0; i < p.size(); i++) {
		parameter_space.at(i).AddInterval(p.at(i));
	}
}

template <typename value_type>
bool paramset<value_type>::isEmpty() {

	bool empty = true;
	for (size_t i = 0; i < (this->parameter_space).size(); i++) {
		if ((this->parameter_space.at(i)).GetIntervals().size()>0) {
			empty = false;
		}
	}
	return empty;
}
